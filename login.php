<?php

session_start();

$error_message = "";

if(isset($_POST["login"])) {
	if(empty($_POST["userid"])) {
		$error_message = "username is missing";
	} else if(empty($_POST["password"])) {
		$error_message = "password is missing";
	}

	if(!empty($_POST["userid"]) && !empty($_POST["password"])) {
		$mysql = mysqli_connect('localhost', 'test', 'testpass');
		if(!$mysql) {
			exit();
		}
		$db_selected = mysqli_select_db('blog_db', $mysql);
		if (!$db_selected){
		    exit();
		}

		$result = mysqli_query("SELECT * FROM db_user WHERE name = '" . $userid . "'");
		while ($row = mysqli_fetch_assoc($result)) {
			$partinent_pass = $row["password"];
		}

    mysqli_close($mysql);


		if(password_verify($_POST["password"], $partinent_pass)) {
			session_regenerate_id(true);
		  $_SESSION["USERID"] = $_POST["userid"];
		  header("Location: admin.php");
		  exit();
		} else {
			$error_message = "password is mistaken";
		}
	}
}

?>

<!DOCTYPE html>
<html>
  <head>
  <meta charset="UTF-8">
  <title>管理画面-ログイン-</title>
  </head>
  <body>
  	<h1>ブログツール 管理画面</h1>
  	<form id="loginForm" name="loginForm" action="" method="POST">
  	<div><?php echo $errorMessage ?></div>
  	<label for="userid">ユーザID</label><input type="text" id="userid" name="userid" value="<?php echo htmlspecialchars($_POST["userid"], ENT_QUOTES); ?>">
  	<br>
  	<label for="password">パスワード</label><input type="password" id="password" name="password" value="">
  	<br>
  	<input type="submit" id="login" name="login" value="ログイン">
  	</form>
  </body>
</html>